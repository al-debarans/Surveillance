import cv2
import time

stream = cv2.VideoCapture(0)
while True:
	check, frame = stream.read()
	key = cv2.waitKey(1)
	cv2.imshow("stream", frame)
	if key == ord('q'):
		break
	elif key == ord('t'):
		cv2.imwrite("capture.jpg", frame)
		print("picture saved")

print("destroy all window")
stream.release()
cv2.destroyAllWindows()
