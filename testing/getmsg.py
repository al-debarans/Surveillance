# RPi
import cv2
import time
import paho.mqtt.client as mqtt
import RPi.GPIO as GPIO

# Setup callback functions that are called when MQTT events happen like 
# connecting to the server or receiving data from a subscribed feed. 
def on_connect(client, userdata, flags, rc):
	print("Connected with result code " + str(rc))
   # Subscribing in on_connect() means that if we lose the connection and 
   # reconnect then subscriptions will be renewed.
	client.subscribe("/data/esp8266")
# The callback for when a PUBLISH message is received from the server. 
def on_message(client, userdata, msg):
	print("Message from nodeMCU\n")
	print("Message topic : "+msg.topic+"\nMessage : "+str( msg.payload))
   # Check if this is a message for the Pi LED.
	if msg.topic == '/data/esp8266':
       # Look at the message data and perform the appropriate action. 
		if msg.payload == b'detected':
			cv2.imwrite("capture.jpg", frame)
			print("picture saved")
# Create MQTT client and connect to localhost, i.e. the Raspberry Pi running 
# this script and the MQTT server.
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect('localhost', 1883, 60)
# Connect to the MQTT server and process messages in a background thread. 
client.loop_start()


# init for camera

#stream = cv2.VideoCapture(0)
while True:
	time.sleep(10)
"""	check, frame = stream.read()
	key = cv2.waitKey(1)
	cv2.imshow("stream", frame)
	if key == ord('q'):
		break

print("destroy all window")
stream.release()
cv2.destroyAllWindows()
"""
