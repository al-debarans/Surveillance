from __future__ import print_function
from imutils.object_detection import non_max_suppression
from imutils import paths
import numpy as np
import argparse
import imutils
import cv2
import time

path = 'tes1.jpg'

def det(path):
    hog = cv2.HOGDescriptor()
    hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())

    image = cv2.imread(path)
    image = imutils.resize(image, width=min(400, 400))
    # detect
    (rects, weight) = hog.detectMultiScale(image, winStride=(4,4), padding=(8,8), scale=1.05)
    print(rects)
    if len(weight) != 0:
        print("ada manusia")
        #return True
    else:
        print("tidak ada")
        #return False
    # draw the original bounding boxes

    for (x, y, w, h) in rects:
        cv2.rectangle(image, (x, y), (x + w, y + h), (0, 0, 255), 2)

    rects = np.array([[x, y, x + w, y + h] for (x, y, w, h) in rects])
    pick = non_max_suppression(rects, probs=None, overlapThresh=0.65)

    # draw the final bounding boxes
    for (xA, yA, xB, yB) in pick:
        cv2.rectangle(image, (xA, yA), (xB, yB), (0, 255, 0), 2)

    while True:
        key = cv2.waitKey(1)
        cv2.imshow("capture", image)
        if key == ord('q'):
            break


if __name__ == '__main__':
	det(path)
