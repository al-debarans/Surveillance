#coder :- Salman Faris

import sys
import time
import json
import telepot
import RPi.GPIO as GPIO

token = ''
bot = telepot.Bot(token)
#LED
def on(pin):
        GPIO.output(pin,GPIO.HIGH)
        return
def off(pin):
        GPIO.output(pin,GPIO.LOW)
        return
# to use Raspberry Pi board pin numbers
GPIO.setmode(GPIO.BOARD)
# set up GPIO output channel
GPIO.setup(11, GPIO.OUT)

def handle(msg):
    chat_id = msg['chat']['id']
    user = msg['chat']['username']
    command = msg['text']

    print('Got command: %s' % command)
    #print(msg)

    if command == '/start':
        saveUser(chat_id, user)
        print("chat id " + readUser())        
            
    elif command =='Hello':
       bot.sendMessage(chat_id, 'Hai')

def saveUser(chatId, username):
    data = {}
    data['user'] = []
    data['user'].append({
        'chat_id': chatId,
        'username': username
       })
    with open('user.json', 'w') as outfile:
        json.dump(data, outfile)

def readUser():
    with open('user.json') as json_file:
        data = json.load(json_file)
        for chatid in data['user']:
            chat_id = str(chatid['chat_id'])
    return chat_id

def main():
	bot.message_loop(handle)
	print('I am listening...')

	while 1:
		try:
			time.sleep(10)

		except KeyboardInterrupt:
			print('\n Program interrupted')
			GPIO.cleanup()
			exit()

		except:
			print('Other error or exception occured!')
			GPIO.cleanup()

if __name__ == '__main__':
	main()
