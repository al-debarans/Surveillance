#include <ESP8266WiFi.h>
#include <PubSubClient.h>


/************************* WiFi Access Point *********************************/
#define wlan_ssid   "Berkah"
#define wlan_pass   "duniasehat"

// defining mqtt profile
const char* mqttServerAddr = "192.168.100.88";
const char* mqttServerAddr2 = "192.168.100.88";
const int mqttPort = 1883;
const char* mqttUser = "";
const char* mqttPass = "";
WiFiClient espClient;
PubSubClient client(espClient);

// D4 = pinout nomor 2
int countWifi = 0;
int led = 5;
int ledesp = 2;
int sensor = 13;
unsigned char looped;

void setup(void)
{
  Serial.begin(115200);
  pinMode(ledesp, OUTPUT);
  pinMode(led, OUTPUT);
  pinMode(sensor, INPUT);
  WiFi.begin(wlan_ssid, wlan_pass);

  // Trying to connect to the network
  while (WiFi.status() != WL_CONNECTED) {
    Serial.println("Waiting to connect to the network..");
    delay(500);
  }

  Serial.println("\n##################################");
  Serial.print("Connected to "); Serial.println(wlan_ssid);
  Serial.print("Your IP Addr is : "); Serial.println(WiFi.localIP());
  
  client.setServer(mqttServerAddr, mqttPort);
  client.setCallback(callback);
  while (!client.connected()) {
    Serial.println("Connecting to MQTT...");

    if (client.connect("ESP8266Client", mqttUser, mqttPass )) {

      Serial.println("connected");
      for (looped = 0; looped < 3; looped++)
      {
        digitalWrite(ledesp, HIGH);
        delay(200);
        digitalWrite(ledesp, LOW);
        delay(200);
      }

    } else {

      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);

    }
  }
}

void callback(char* topic, byte* payload, unsigned int length) {

  Serial.print("Message arrived in topic: ");
  Serial.println(topic);

  Serial.print("Message:");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }

  Serial.println();
  Serial.println("-----------------------");

}

void loop() {

  long val = digitalRead(sensor);
  client.loop();
  if (val == HIGH) {
    digitalWrite(ledesp, HIGH);
    Serial.println("publish");
    client.publish("/data/esp8266", "triggered");
    delay(3000);
  }
  else {
    digitalWrite(ledesp, LOW);
    Serial.println(val);
    delay(500);
  }
  /*
    if (Serial.available() > 0) {
    // read incoming serial data:
    char inChar = Serial.read();
    // Type the next ASCII value from what you received:
    if (inChar == 's') {
      Serial.println("publish");
      client.publish("/data/esp8266", "triggered");
    }
    else if (inChar == 'r')
    {
      Serial.println("receive from pi");
      client.subscribe("raspi");
    }
    }
    delay(300);
    client.loop();
    //  client.publish("/data/esp8266", "detected");
    //  client.subscribe("raspi");
    //  delay(300);
    //  client.loop();*/
}
