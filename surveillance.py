# import lib
from imutils.object_detection import non_max_suppression
from imutils import paths
import cv2
import sys
import time
import json
import logging
import telepot
import imutils
import datetime
import numpy as np
import paho.mqtt.client as mqtt
import RPi.GPIO as GPIO

detectionHuman = False

# init token bot telegram
token = ''
bot = telepot.Bot(token)

# init formatter untuk log file
formatter = logging.Formatter('[%(asctime)s] - %(levelname)s - %(message)s')

# init for camera
stream = cv2.VideoCapture(0)

# setup topic pesan yang akan di subcribe
def on_connect(client, userdata, flags, rc):
	print("Connected on mqtt server with result code " + str(rc))
	# subscribing topic
	client.subscribe("/data/esp8266")

# handle pesan yang masuk dari mqtt
def on_message(client, userdata, msg):
	print("Request topic: " + msg.topic + " |Message: " + str(msg.payload))
	print("\n###############################################\n")
	currentTime = datetime.datetime.now()
	if msg.topic == '/data/esp8266':
		if msg.payload == b'triggered':
			print("ada gerakan")
			pictName = "capture/IMG_" + currentTime.strftime('%Y%m%d%H%M%S') +".jpg"
			print(pictName)
			cv2.imwrite(pictName, frame)
			
			logger = setup_logger('detect', 'log/detect.log')
			msg = "Terdeteksi adanya gerakan. Hasil capture : " + pictName
			logger.warning(msg)
			
			print(frame)
	# setelah capture lakukan deteksi menggunakan HOG & SVM
			status = detection(pictName)

			if status == True:
				msg = "terdeteksi ada manusia di ruangan"
				logger.warning(msg)
				chat = "bos ada maling"
				logger = setup_logger('telegrambot', 'log/telegrambot.log')
				bot.sendMessage(readUser(),chat)
				bot.sendPhoto(readUser(),open(pictName,'rb'))
				msg = "mengirim info gambar kondisi ruangan"
				logger.info(msg)
			else:
				msg = "tidak ada pergerakan manusia"
				logger.info(msg)

# handle telegram message
def telegram_msg(msg):
	chat_id = msg['chat']['id']
	command = msg['text']
	user = msg['chat']['username']

    # print request message from user
	print('Got command: %s' %command)
	if command == '/start':
		saveUser(chat_id,user)
		chat = "Hallo " + user
		bot.sendMessage(chat_id,chat)

	elif command == '-monitoring':
		bot.sendMessage(chat_id,'Tunggu sebentar proses pengambilan gambar')
		cv2.imwrite("telegrambot.jpg", frame)
		bot.sendPhoto(chat_id,open('telegrambot.jpg','rb'))
		bot.sendMessage(chat_id, 'Kondisi ruang saat ini')
	# tambahkan perintah untuk attachment file gambar

def saveUser(chatId, username):
    data = {}
    data['user'] = []
    data['user'].append({
        'chat_id': chatId,
        'username': username
       })
    with open('user.json', 'w') as outfile:
        json.dump(data, outfile)

def readUser():
    with open('user.json') as json_file:
        data = json.load(json_file)
        for chatid in data['user']:
            chat_id = str(chatid['chat_id'])
    return chat_id

def detection(path):
	hog = cv2.HOGDescriptor()
	hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())

	image = cv2.imread(path)
	image = imutils.resize(image, width=min(400, 400))

	# proses mendeteksi
	(rects, weight) = hog.detectMultiScale(image, winStride=(4,4), padding=(8,8), scale=1.05)

	if len(weight) != 0:
		print("ada manusia")
		return True
	else:
		print("tidak ada")
		return False

def setup_logger(name, log_file, level=logging.INFO):

	handler = logging.FileHandler(log_file)
	handler.setFormatter(formatter)

	logger = logging.getLogger(name)
	logger.setLevel(level)
	logger.addHandler(handler)

	return logger


def main():
        # menjalankan service mqtt
	client = mqtt.Client()
	client.on_connect = on_connect
	client.on_message = on_message
	client.connect('localhost', 1883,60)
	client.loop_start()
	print("mqtt subscribe is running")

	# menjalankan service telegrambot
	bot.message_loop(telegram_msg)
	print('Telegram bot is activated')
	
if __name__ == '__main__':
	main()
	while True:
#		time.sleep(5)
		check, frame = stream.read()
#		cv2.imshow("capture", frame)
#		key = cv2.waitKey(1)
#		if key == ord('q'):
#			break
#		elif key == ord('t'):
#			cv2.imwrite("capture.jpg", frame)
#			print(frame)
#	print("all service stoped")
#	stream.release()
#	cv2.destroyAllWindows()
